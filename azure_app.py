import streamlit as st
from openai import AzureOpenAI
from dotenv import load_dotenv
import os


if load_dotenv():
    print("Found OpenAI API Base Endpoint: " + os.getenv("AZURE_OPENAI_ENDPOINT"))
else: 
    print("OpenAI API Base Endpoint not found. Have you configured the .env file?")
    

st.title("My ChatGPT")

client = AzureOpenAI(
    azure_endpoint = os.getenv("AZURE_OPENAI_ENDPOINT"), 
    api_key=os.getenv("AZURE_OPENAI_KEY"),  
    api_version=os.getenv("OPENAI_API_VERSION")
    
)
if "openai_model" not in st.session_state:
    st.session_state["openai_model"] = os.getenv("OPENAI_COMPLETION_MODEL")  # Or your preferred model

if "messages" not in st.session_state:
    st.session_state.messages = []

for message in st.session_state.messages:
    with st.chat_message(message["role"]):
        st.markdown(message["content"])

if prompt := st.chat_input("What is up?"):
    st.session_state.messages.append({"role": "user", "content": prompt})
    with st.chat_message("user"):
        st.markdown(prompt)

    with st.chat_message("assistant"):
        message_placeholder = st.empty()
        full_response = ""
        response = client.chat.completions.create(
        model=os.getenv("OPENAI_COMPLETION_MODEL"), 
        messages=[
            {"role": m["role"], "content": m["content"]}
            for m in st.session_state.messages
        ])
        full_response += (response.choices[0].message.content or "")
    
        message_placeholder.markdown(full_response + "▌")
    st.session_state.messages.append({"role": "assistant", "content": full_response})
